# example-ml-project

## Installation

on your machine, should be [dvc](https://dvc.org/doc/get-started/install) already installed

```bash
pip install dvc
```

## Workflow

init a dvc project, this will create .dvc in current directory and put .dvc into .gitignore
```bash
dvc init -v
```


and a remote backend like s3 bucket provisioned and connected to dvc

```bash
dvc remote
```
## Pipeline

data_preparation.dvc

```bash
dvc run -d data/iris.csv -o data/iris_X_train.csv -o data/iris_Y_train.csv -o data/iris_Y_test.csv -o data/iris_X_test.csv -f data_preparation.dvc python code/data_preparation.py
```
model_selection.dvc

```bash
dvc run -d data/iris_X_train.csv -d data/iris_Y_train.csv -o model/model.pickle -f model_selection.dvc python code/model_selection.py
```
model_evaluation.dvc

```bash
dvc run -d data/iris_X_test.csv -d data/iris_Y_test.csv -d model/model.pickle -M auc.metric -f model_evaluation.dvc python code/model_evaluation.py
```

