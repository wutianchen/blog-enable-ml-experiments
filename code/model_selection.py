from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.metrics import accuracy_score
import pandas as pd
import pickle

seed = 7
scoring = 'accuracy'

iris_X_train = pd.read_csv("data/iris_X_train.csv", index_col=0)
iris_Y_train = pd.read_csv("data/iris_Y_train.csv", index_col=0)

# model
models = []
models.append(('LR', LogisticRegression(solver='liblinear', multi_class='ovr')))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC(gamma='auto')))

# evaluate each model in turn
results = []
names = []
for name, model in models:
    kfold = model_selection.KFold(n_splits=10, random_state=seed)
    cv_results = model_selection.cross_val_score(model, iris_X_train, iris_Y_train, cv=kfold, scoring=scoring)
    results.append(cv_results.mean())
    names.append(name)

# select the model with best average performance
min_index = results.index(min(results))
model = models[min_index][1]
print(models[min_index][0])


final_model = model.fit(iris_X_train, iris_Y_train)
pickle.dump(final_model, open("model/model.pickle", 'wb'))
