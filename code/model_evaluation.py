from sklearn.metrics import accuracy_score
import pickle
import pandas as pd

with open("model/model.pickle", "rb") as file:
    final_model = pickle.load(file)

iris_X_test = pd.read_csv("data/iris_X_test.csv", index_col=0)
iris_Y_test = pd.read_csv("data/iris_Y_test.csv", index_col=0)

predictions = final_model.predict(iris_X_test)
metric = accuracy_score(iris_Y_test, predictions)
print(metric)

with open("auc.metric", "w") as f:
    f.write(str(metric))
# print(confusion_matrix(iris_Y_test, predictions))
# print(classification_report(iris_Y_test, predictions))
