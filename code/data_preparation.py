import pandas as pd
from sklearn import model_selection


seed = 7
validation_size = 0.20

iris_data = pd.read_csv("data/iris.csv", index_col=0)
iris_X = iris_data[["sepal length (cm)", "sepal width (cm)", "petal length (cm)", "petal width (cm)"]]
iris_Y = iris_data["target"]
iris_X_train, iris_X_test, iris_Y_train, iris_Y_test = model_selection.train_test_split(iris_X, iris_Y, test_size=validation_size, random_state=seed)

iris_X_train.to_csv("data/iris_X_train.csv", header=False)
iris_X_test.to_csv("data/iris_X_test.csv", header=False)
iris_Y_train.to_csv("data/iris_Y_train.csv")
iris_Y_test.to_csv("data/iris_Y_test.csv")
